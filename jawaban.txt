1. buat database 
create database myshop;

2. cara buat table 
- users
create table users(
    	id int(4) AUTO_INCREMENT PRIMARY KEY,
    	nama varchar(255) NOT null,
    	email varchar(255) NOT null,
    	PASSWORD varchar(255) NOT null
    );

- categories
create table categories(
    	id int(4) AUTO_INCREMENT PRIMARY KEY,
    	nama varchar(255) NOT null
    );

- items
create table items(
    	id int(4) AUTO_INCREMENT PRIMARY KEY,
    	nama varchar(255) NOT null,
    	description varchar(255) NOT null,
    	price int(4) NOT null,
    	stock int(4) NOT null,
    	category_id int(4) NOT null, 
    	FOREIGN KEY (category_id) REFERENCES categories(id)
    );


3. insert
- users
INSERT INTO users(nama, email, PASSWORD) values("John Doe", "john@doe.com", "john123");

INSERT INTO users(nama, email, PASSWORD) values("Jane Doe", "jane@doe.com", "jenita123");


- categories
INSERT INTO categories(nama) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded");

- item
INSERT INTO items(nama, description, price, stock, category_id) VALUES("Sumsang b50",	"hape keren dari merek sumsang", "4000000", "100","1");


INSERT INTO items(nama, description, price, stock, category_id) VALUES("Uniklooh", "baju keren dari brand ternama", "500000",  "50",	"2");

INSERT INTO items(nama, description, price, stock, category_id) VALUES("IMHO", " Watch	jam tangan anak yang jujur banget",	 "2000000",  "10",	"2");

4. menampilkan 
a. tampilkan kecual password
SELECT nama, email  FROM users ;

b. Mengambil data items

- Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

SELECT * from items WHERE price > 1000000;

- Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).

SELECT * FROM `items` WHERE nama LIKE '%sang%';

Soal 5 Mengubah Data dari Database
Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000. Masukkan query pada text jawaban di nomor ke 5.



UPDATE items set price=2500000 WHERE  id= 1;



